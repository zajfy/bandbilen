#include <Arduino.h>
#ifndef LINKEDLIST_H

class Node
{
public:
    Node* next;
	Node* previous;
    int data;
};

using namespace std;

class LinkedList
{
public:
    int length;
    Node* head;
	Node* tail;
	Node* reader;

    LinkedList();
    ~LinkedList();
    void add(int data);
	void remove();
	int getMean();
};
#endif