//#include "linkedlist.h"

#include <Arduino.h>

class Node
{
public:
    Node* next;
	Node* previous;
    int data;
};

using namespace std;

class LinkedList
{
public:
    int length;
    Node* head;
	Node* tail;
	Node* reader;

    LinkedList();
    ~LinkedList();
    void add(int data);
	void remove();
	int getMean();
};

LinkedList::LinkedList()
{
    this->length = 0;
    this->head = NULL;
}

LinkedList::~LinkedList()
{
	
}

void LinkedList::add(int data)
{
	
    Node* node = new Node();
    node->data = data;
    node->next = this->head;
	if(this->head == NULL)
	{
		this->tail = node;
	}
    this->head = node;
    this->length++;
}
void LinkedList::remove()
{
	Node* node = this->head;
	while(node->next->next != NULL)
	{
		node = node->next;
	}
	this->tail = node;
	delete this->tail->next;
	this->tail->next = NULL;
	this->length--;
}

int LinkedList::getMean()
{
	int temp = 0;
	Node* node = this->head;
	while(node != NULL)
	{
		if(node->data > 200)
		{
			node->data = 200;
		}
		temp += node->data;
	}
	temp = temp / this->length;
	return temp;
}

